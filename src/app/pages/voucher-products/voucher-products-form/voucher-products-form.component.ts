import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormComponent } from 'src/app/shared/components/base-resource-form/base-resource-form.component';

import { VoucherProduct } from '../shared/voucher-products.model';
import { VoucherProductService } from '../shared/voucher-products.service';

@Component({
  selector: 'app-voucherProduct-form',
  templateUrl: './voucher-products-form.component.html',
  styleUrls: ['./voucher-products-form.component.css'],
})
export class VoucherProductFormComponent extends BaseResourceFormComponent<VoucherProduct> {

  voucherProduct: VoucherProduct = new VoucherProduct();

  constructor(
    protected voucherService: VoucherProductService,
    protected override injector: Injector
  ) {
    super(injector, new VoucherProduct(), voucherService, VoucherProduct.fromJson);
  }

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      code:[null],
      voucher_code: [null, [Validators.required]],
      product_code: [null, [Validators.required]],
    });
  }

  protected override creationPageTitle(): string {
    return 'Cadastro de novo Voucher Product';
  }
  protected override editionPageTitle(): string {
    const voucherName = this.resource.code || '';
    return 'Editando o voucher: ' + voucherName;
  }




}
