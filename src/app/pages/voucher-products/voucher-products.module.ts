import { SharedModule } from './../../shared/shared.module';
import { VoucherProductFormComponent } from './voucher-products-form/voucher-products-form.component';
import { NgModule } from '@angular/core';

import { VoucherProductListComponent } from './voucher-products-list/voucher-products-list.component';
import { VoucherProductsRoutingModule } from './voucher-products-routing.module';


@NgModule({
  declarations: [
    VoucherProductListComponent,
    VoucherProductFormComponent
  ],
  imports: [
    VoucherProductsRoutingModule,
    SharedModule
  ]
})
export class VoucherProductsModule { }
