import { VoucherProductService } from '../shared/voucher-products.service';
import { Component, OnInit } from '@angular/core';
import { VoucherProduct } from '../shared/voucher-products.model';
import { BaseResourceListComponent } from 'src/app/shared/components/base-resource-list/base-resource-list.component';

@Component({
  selector: 'app-voucherProduct-list',
  templateUrl: './voucher-products-list.component.html',
  styleUrls: ['./voucher-products-list.component.css']
})
export class VoucherProductListComponent extends BaseResourceListComponent<VoucherProduct> {

  constructor(private voucherProductService: VoucherProductService) {
    super(voucherProductService);
  }

}
