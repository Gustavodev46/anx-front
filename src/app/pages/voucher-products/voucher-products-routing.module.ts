import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VoucherProductFormComponent } from './voucher-products-form/voucher-products-form.component';
import { VoucherProductListComponent } from './voucher-products-list/voucher-products-list.component';

const routes: Routes = [
  {path: '', component: VoucherProductListComponent},
  {path: 'new', component: VoucherProductFormComponent},
  {path: ':code/edit', component: VoucherProductFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoucherProductsRoutingModule { }
