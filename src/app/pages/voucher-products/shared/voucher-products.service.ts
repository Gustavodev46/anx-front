import { Injectable, Injector } from '@angular/core';
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';

import { VoucherProduct } from './voucher-products.model';

@Injectable({
  providedIn: 'root',
})
export class VoucherProductService extends BaseResourceService<VoucherProduct> {
  constructor(protected override injector: Injector) {
    super('http://localhost:3000/voucher-products', injector, VoucherProduct.fromJson);
  }
}
