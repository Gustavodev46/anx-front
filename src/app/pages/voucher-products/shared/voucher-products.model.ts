import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

import { Voucher } from './../../vouchers/shared/voucher.model';

export class VoucherProduct extends BaseResourceModel {
  override code!: number;
  voucher_code!: number;
  product_code!: string;
  voucher!: Voucher;

  static fromJson(json: any): VoucherProduct {
    return Object.assign(new VoucherProduct(), json);
  }
}
