import { Injectable, Injector } from '@angular/core';
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';
import { Voucher } from './voucher.model';

@Injectable({
  providedIn: 'root',
})
export class VoucherService extends BaseResourceService<Voucher> {
  constructor(protected override injector: Injector) {
    super('http://localhost:3000/vouchers', injector, Voucher.fromJson);
  }
}
