import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

export class Voucher extends BaseResourceModel {
  override code!: number;
  discount_campaign!: string;
  type!: string;
  discount!: string;
  start_date!: Date;
  end_date!: Date;
  minimum_sale!: number;
  quantity!: number;
  prefix!: string;
  random_number!: string;
  quantity_use!: number;
  linked_code!: number;
  status!: string;

  static fromJson(json: any): Voucher {
    return Object.assign(new Voucher(), json);
  }
}
