import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormComponent } from 'src/app/shared/components/base-resource-form/base-resource-form.component';
import { Voucher } from '../shared/voucher.model';
import { VoucherService } from '../shared/voucher.service';

@Component({
  selector: 'app-voucher-form',
  templateUrl: './voucher-form.component.html',
  styleUrls: ['./voucher-form.component.css'],
})
export class VoucherFormComponent extends BaseResourceFormComponent<Voucher>{

  voucher: Voucher = new Voucher();

  constructor( protected voucherService: VoucherService, protected override injector: Injector) {
    super(injector, new Voucher(), voucherService, Voucher.fromJson)
  }

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      code:[null],
      discount_campaign: [null, [Validators.required]],
      type: [null, [Validators.required]],
      discount: [null, [Validators.required]],
      start_date: [null, [Validators.required]],
      end_date: [null, [Validators.required]],
      minimum_sale: [null, [Validators.required]],
      quantity: [null, [Validators.required]],
      prefix: [null, [Validators.required]],
      random_number: [null, [Validators.required]],
      quantity_use: [null],
      linked_code: [null],
      status: [null, [Validators.required]],
    });
  }

  protected override creationPageTitle(): string {
      return "Cadastro de novo Voucher"
  }
  protected override editionPageTitle(): string {
      const voucherName = this.resource.discount_campaign || ""
      return " Editando o voucher: " + voucherName
  }
}
