import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';

import { VouchersRoutingModule } from './vouchers-routing.module';
import { VoucherListComponent } from './voucher-list/voucher-list.component';
import { VoucherFormComponent } from './voucher-form/voucher-form.component';


@NgModule({
  declarations: [
    VoucherListComponent,
    VoucherFormComponent
  ],
  imports: [
    VouchersRoutingModule,
    SharedModule
  ]
})
export class VouchersModule { }
