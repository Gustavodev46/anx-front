import { Component } from '@angular/core';
import { BaseResourceListComponent } from 'src/app/shared/components/base-resource-list/base-resource-list.component';

import { Voucher } from '../shared/voucher.model';
import { VoucherService } from './../shared/voucher.service';

@Component({
  selector: 'app-voucher-list',
  templateUrl: './voucher-list.component.html',
  styleUrls: ['./voucher-list.component.css'],
})
export class VoucherListComponent extends BaseResourceListComponent<Voucher> {
  constructor(private voucherService: VoucherService) {
    super(voucherService);
  }
}
