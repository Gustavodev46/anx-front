import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VoucherFormComponent } from './voucher-form/voucher-form.component';
import { VoucherListComponent } from './voucher-list/voucher-list.component';

const routes: Routes = [
 { path: '', component: VoucherListComponent},
 { path: 'new', component: VoucherFormComponent},
 { path: ':code/edit', component: VoucherFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VouchersRoutingModule { }
