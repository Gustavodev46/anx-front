import { SharedModule } from './../../shared/shared.module';
import { VoucherStoreListComponent } from './voucher-stores-list/voucher-stores-list.component';
import { VoucherStoreFormComponent } from './voucher-stores-form/voucher-stores-form.component';
import { NgModule } from '@angular/core';
import { VoucherStoresRoutingModule } from './voucher-stores-routing.module';



@NgModule({
  declarations: [
    VoucherStoreListComponent,
    VoucherStoreFormComponent
  ],
  imports: [
    VoucherStoresRoutingModule,
    SharedModule
  ]
})
export class VoucherStoresModule { }
