import { Injectable, Injector } from '@angular/core';
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';

import { VoucherStore } from './voucher-stores.model';

@Injectable({
  providedIn: 'root',
})
export class VoucherStoreService extends BaseResourceService<VoucherStore> {
  constructor(protected override injector: Injector) {
    super('http://localhost:3000/voucher-stores', injector, VoucherStore.fromJson);
  }
}
