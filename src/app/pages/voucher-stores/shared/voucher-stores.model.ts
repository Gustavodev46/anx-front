import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

import { Voucher } from './../../vouchers/shared/voucher.model';

export class VoucherStore extends BaseResourceModel {
  override code!: number;
  voucher_code!: number;
  store_code!: string;
  voucher!: Voucher;

  static fromJson(json: any): VoucherStore {
    return Object.assign(new VoucherStore(), json);
  }
}
