import { Component } from '@angular/core';
import { BaseResourceListComponent } from 'src/app/shared/components/base-resource-list/base-resource-list.component';

import { VoucherStore } from '../shared/voucher-stores.model';
import { VoucherStoreService } from '../shared/voucher-stores.service';

@Component({
  selector: 'app-voucherStore-list',
  templateUrl: './voucher-stores-list.component.html',
  styleUrls: ['./voucher-stores-list.component.css'],
})
export class VoucherStoreListComponent extends BaseResourceListComponent<VoucherStore> {
  constructor(private voucherStoreService: VoucherStoreService) {
    super(voucherStoreService);
  }
}
