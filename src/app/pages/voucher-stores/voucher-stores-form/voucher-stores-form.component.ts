import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormComponent } from 'src/app/shared/components/base-resource-form/base-resource-form.component';

import { VoucherStore } from '../shared/voucher-stores.model';
import { VoucherStoreService } from '../shared/voucher-stores.service';

@Component({
  selector: 'voucher-stores-form',
  templateUrl: './voucher-stores-form.component.html',
  styleUrls: ['./voucher-stores-form.component.css'],
})
export class VoucherStoreFormComponent extends BaseResourceFormComponent<VoucherStore> {
  voucher: VoucherStore = new VoucherStore();

  constructor(
    protected voucherService: VoucherStoreService,
    protected override injector: Injector
  ) {
    super(injector, new VoucherStore(), voucherService, VoucherStore.fromJson);
  }

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      code: [null],
      voucher_code: [null, [Validators.required]],
      store_code: [null, [Validators.required]],
    });
  }

  protected override creationPageTitle(): string {
    return 'Cadastro de novo Voucher Store';
  }
  protected override editionPageTitle(): string {
    const voucherName = this.resource.code || '';
    return 'Editando o voucher: ' + voucherName;
  }
}
