import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VoucherStoreFormComponent } from './voucher-stores-form/voucher-stores-form.component';
import { VoucherStoreListComponent } from './voucher-stores-list/voucher-stores-list.component';

const routes: Routes = [
  {path: '', component: VoucherStoreListComponent},
  {path: 'new', component: VoucherStoreFormComponent},
  {path: ':code/edit', component: VoucherStoreFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoucherStoresRoutingModule { }
