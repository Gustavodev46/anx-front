import { VouchersModule } from './pages/vouchers/vouchers.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {    path: 'vouchers',loadChildren: () => import('./pages/vouchers/vouchers.module').then(m => m.VouchersModule) },
  {    path: 'voucher-products',loadChildren: () => import('./pages/voucher-products/voucher-products.module').then(m => m.VoucherProductsModule) },
  {    path: 'voucher-stores',loadChildren: () => import('./pages/voucher-stores/voucher-stores.module').then(m => m.VoucherStoresModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
