import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';
import { Directive, OnInit } from '@angular/core';

import { BaseResourceService } from '../../services/base-resource.service';

@Directive()
export abstract class BaseResourceListComponent<T extends BaseResourceModel> implements OnInit {

  resources: T[] = [];
  constructor(protected resourceService: BaseResourceService<T>) {}

  ngOnInit(): void {
    this.resourceService.getAll().subscribe({
      next: (resources) => (this.resources = resources),
      error: () => alert('Erro ao carregar a lista'),
    });
  }

  deleteResource(resource: T) {
    const mustDelete = confirm('Deseja realmente excluir este item?');
    if (mustDelete)
      this.resourceService.delete(resource.code).subscribe({
        next: () =>
          (this.resources = this.resources.filter(
            (element) => element != resource
          )),
        error: () =>
          alert(
            'ERRO AO TENTAR EXCLUIR, VOUCHER VINCULADO A UMA LOJA OU PRODUTO'
          ),
      });
  }
}
