import {
  AfterContentChecked,
  Directive,
  Injector,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { BaseResourceModel } from '../../models/base-resource.model';
import { BaseResourceService } from '../../services/base-resource.service';

@Directive()
export abstract class BaseResourceFormComponent<T extends BaseResourceModel>
  implements OnInit, AfterContentChecked
{
  currentAction!: string;
  resourceForm!: FormGroup;
  pageTitle!: string;
  serverErrorMessages?: string[];
  submittingForm: boolean = false;
  fileToUpload!: File;

  protected route!: ActivatedRoute;
  protected router!: Router;
  protected formBuilder!: FormBuilder;

  constructor(
    protected injector: Injector,
    public resource: T,
    protected resourceService: BaseResourceService<T>,
    protected jsonDataToResourceFn: (json: any) => T
  ) {
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.formBuilder = this.injector.get(FormBuilder);
  }

  ngOnInit() {
    this.setCurrentAction();
    this.buildResourceForm();
    this.loadResource();
  }

  ngAfterContentChecked() {
    this.setPageTitle();
  }

  submitForm() {
    this.submittingForm = true;

    if (this.currentAction == 'new') this.createResource();
    // currentAction == "edit"
    else this.updateResource();
  }

  // PRIVATE METHODS

  protected setCurrentAction() {
    if (this.route.snapshot.url[0].path == 'new') this.currentAction = 'new';
    else this.currentAction = 'edit';
  }

  protected loadResource() {
    if (this.currentAction == 'edit') {
      this.route.paramMap
        .pipe(
          switchMap((params) =>
            this.resourceService.getByCode(+params.get('code')!)
          )
        )
        .subscribe({
          next: (resource) => {
            this.resource = resource;
            this.resourceForm.patchValue(resource); // binds loaded resource data to BaseResourceForm
          },
          error: (error) =>
            alert('Ocorreu um erro no servidor, tente mais tarde.'),
        });
    }
  }

  protected setPageTitle() {
    if (this.currentAction == 'new') this.pageTitle = this.creationPageTitle();
    else {
      const resourceCode = this.resource.code || '';
      this.pageTitle = this.editionPageTitle();
    }
  }

  protected creationPageTitle(): string {
    return 'New';
  }
  protected editionPageTitle(): string {
    return 'Update';
  }

  protected createResource() {
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);
    this.resourceService.create(resource).subscribe({
      next: (resource) => this.actionsForSuccess(resource),
      error: (error) => this.actionsForError(error),
    });
  }

  // protected createResourcesByExcel() {
  //   this.resourceService.postFile(this.fileToUpload).subscribe({
  //     next: (resource) => this.actionsForSuccess(resource),
  //     error: (error) => this.actionsForError(error),
  //   });
  // }
  // handleFileInput(files: FileList) {
  //     this.fileToUpload = files.item(0);
  // }

  protected updateResource() {
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);
    const code: number = this.route.snapshot.params['code'];
    resource.code = code;
    this.resourceService.update(resource).subscribe({
      next: (resource) => this.actionsForSuccess(resource),
      error: (error) => this.actionsForError(error),
    });
  }

  protected actionsForSuccess(resource: T) {
    alert('Solicitação processada com sucesso!');
    const baseComponentPath: any = this.route.snapshot.parent?.url[0].path;
    // redirect/reload component page
    this.router
      .navigateByUrl(baseComponentPath, { skipLocationChange: true })
      .then(() =>
        this.router.navigate([baseComponentPath, resource.code, 'edit'])
      );
  }

  protected actionsForError(error: any) {
    alert('Ocorreu um erro ao processar a sua solicitação!');

    this.submittingForm = false;

    if (error) this.serverErrorMessages = [error];
    else
      this.serverErrorMessages = [
        'Falha na comunicação com o servidor. Por favor, tente mais tarde.',
      ];
  }

  protected abstract buildResourceForm(): void;
}
