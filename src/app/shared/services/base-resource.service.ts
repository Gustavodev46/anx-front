import { HttpClient } from '@angular/common/http';
import { Injector } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { BaseResourceModel } from '../models/base-resource.model';

export abstract class BaseResourceService<T extends BaseResourceModel> {
  protected http!: HttpClient;
  constructor(
    protected api: string,
    protected injector: Injector,
    protected jsonDataToResourceFn: (json: any) => T
  ) {
    this.http = injector.get(HttpClient);
  }

  getAll(): Observable<T[]> {
    return this.http.get(this.api).pipe(
      catchError(this.handleError),
      map(this.jsonDataResources.bind(this)) // esse bind indica que o this não é o map e sim a classe
    );
  }

  getByCode(code: any): Observable<T> {
    const url = `${this.api}/${code}`;
    return this.http
      .get(url)
      .pipe(
        catchError(this.handleError),
        map(this.jsonDataResource.bind(this))
      );
  }

  create(resource: T): Observable<T> {
    return this.http
      .post(this.api, resource)
      .pipe(
        catchError(this.handleError),
        map(this.jsonDataResource.bind(this))
      );
  }

  // postFile(fileToUpload: File): Observable<T> {
  //   const formData: FormData = new FormData();
  //   formData.append('fileKey', fileToUpload, fileToUpload.name);
  //   return this.http
  //     .post(this.api, formData,)
  //     .pipe(
  //       catchError(this.handleError),
  //       map(this.jsonDataResource.bind(this))
  //     );
  // }

  update(resource: T): Observable<T> {
    const url = `${this.api}/${resource.code}`;
    return this.http
      .patch(url, resource)
      .pipe(
        catchError(this.handleError),
        map(this.jsonDataResource.bind(this))
      );
  }

  delete(code: number): Observable<any> {
    const url = `${this.api}/${code}`;
    return this.http.delete(url).pipe(
      catchError(this.handleError),
      map(() => null)
    );
  }

  //PROTECTED METHODS

  protected jsonDataResources(jsonData: any[]): T[] {
    const resources: T[] = [];
    jsonData.forEach((element) =>
      resources.push(this.jsonDataToResourceFn(element))
    );
    return resources;
  }

  protected jsonDataResource(jsonData: any): T {
    return this.jsonDataToResourceFn(jsonData);
  }

  protected handleError(error: any): Observable<any> {
    console.log('ERRO NA REQUISIÇÃO => ', error);
    return throwError(() => new Error(error.error.error));
  }
}
